let express = require('express'),
    router = express.Router(),
    authController = require('../controller/auth'),
    userController = require('../controller/user');

router.get('/approved', authController.userAuthenticate, userController.getApprovedUsers);
router.get('/notApproved', authController.userAuthenticate, userController.getNotApprovedUsers);
router.get('/:userId', authController.userAuthenticate, userController.getSingleUser);
router.patch('/:userId', authController.userAuthenticate, userController.updateUser);
router.delete('/:userId', authController.userAuthenticate, userController.deleteUser);
router.post('/', userController.createUser);
router.get('/', authController.userAuthenticate, userController.getAllUsers);


module.exports = router;