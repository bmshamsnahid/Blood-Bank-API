module.exports = {
    'serverPort': 8080,
    'tokenexp': 3600,
    'secret': 'mysecretkey',
    'analytics': 'UA-107924999-1',
    'database': 'mongodb://localhost:27017/blood-bank-api'
};
