let User = require('../model/user');

let bloodGroups = ['a+', 'a-', 'b+', 'b-', 'o+', 'o-', 'ab+', 'ab-'];

let createUser = (req, res, next) => {

    let email = req.body.email,
        password = req.body.password,
        location = req.body.location,
        bloodGroup = req.body.bloodGroup,
        isApproved = false;

    if (!email) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete email' });
    } else if (!password) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete password' });
    } else if (!location) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete location.' });
    } else if (!bloodGroup) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete blood group.' });
    } else if (bloodGroups.indexOf(bloodGroup.toLowerCase()) < 0) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete blood group' });
    } else {
        let user = new User({
            email: email,
            password: password,
            location: location,
            bloodGroup: bloodGroup,
            isApproved: isApproved,
            name: req.body.name || 'Unknown Name'
        });

        user.save((err, user) => {
            if (err) {
                return res.status(401).json({ success: false, message: err });
            } else if (user) {
                return res.status(201).json({ success: true, message: 'Successfully created the user', data: user });
            }
        });
    }

};

let getSingleUser = (req, res, next) => {
    let userId = req.params.userId;
    if (!userId) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete user id' });
    } else {
        User.findById(userId, (err, user) => {
            if (err) {
                return res.status(401).json({ success: false, message: err });
            } else if (!user) {
                return res.status(202).json({ success: false, message: 'No user found.' });
            } else {
                return res.status(201).json({ success: true, message: 'Successfully get the user data.', data: user });
            }
        });
    }
};

let getAllUsers = (req, res, next) => {

    User.find({}, (err, user) => {
        if (err) {
            return res.status(401).json({ success: false, message: err });
        } else if (user) {
            return res.status(201).json({ success: true, message: 'Successfully get the users data.', data: user });
        }
    });
};

let getApprovedUsers = (req, res, next) => {

    User.find({ isApproved: true }, (err, user) => {
        if (err) {
            return res.status(401).json({ success: false, message: err });
        } else if (user) {
            return res.status(201).json({ success: true, message: 'Successfully get the approved users data.', data: user });
        }
    });
};

let getNotApprovedUsers = (req, res, next) => {

    User.find({ isApproved: false }, (err, user) => {
        if (err) {
            return res.status(401).json({ success: false, message: err });
        } else if (user) {
            return res.status(201).json({ success: true, message: 'Successfully get the not approved users data.', data: user });
        }
    });
};

let updateUser = (req, res, next) => {
    let userId = req.params.userId,
        email = req.body.email,
        password = req.body.password,
        location = req.body.location,
        bloodGroup = req.body.bloodGroup,
        isApproved = req.body.isApproved;
    if (!userId) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete user id' });
    } else if (!email) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete email' });
    } else if (!password) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete password' });
    } else if (!location) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete location.' });
    } else if (!bloodGroup) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete blood group.' });
    } else if (bloodGroups.indexOf(bloodGroup.toLowerCase()) < 0) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete blood group.' });
    } else if (typeof isApproved == 'undefined') {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete approval status.' });
    } else {
        User.findById(userId, (err, user) => {
            if (err) {
                return res.status(401).json({ success: false, message: err });
            } else {
                user.email = email;
                user.password = password;
                user.location = location;
                user.bloodGroup = bloodGroup;
                user.isApproved = isApproved;

                user.save((err, user) => {
                    if (err) {
                        return res.status(401).json({success: false, message: err});
                    } else if (user) {
                        return res.status(201).json({success: true, message: 'Successfully updated the user', data: user});
                    }
                });
            }
        });
    }
};

let deleteUser = (req, res, next) => {
    let userId = req.params.userId;
    if (!userId) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete user id' });
    } else {
        User.findByIdAndRemove(userId, (err, user) => {
            if (err) {
                return res.status(401).json({ success: false, message: err });
            } else if (user) {
                return res.status(201).json({ success: true, message: 'Successfully delete the user data.', data: user });
            }
        });
    }
};

module.exports = {
    createUser,
    getAllUsers,
    getSingleUser,
    getApprovedUsers,
    getNotApprovedUsers,
    updateUser,
    deleteUser
};
